﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Access;
using BankingApp.Common.Models;
using BankingApp.Common.ViewModels;
using BankingApp.Services.Interfaces;

namespace BankingApp.Web.Controllers
{
	[RoutePrefix("account")]
	public class AccountController : ApiController
	{
		private readonly IAccountService _accountService;
		private readonly IAccessController _accessController;

		public AccountController(IAccountService accountService, IAccessController accessController)
		{
			_accountService = accountService;
			_accessController = accessController;
		}

		[Route("Login"), HttpPost]
		public HttpResponseMessage Login(UserLoginModel userLoginModel)
		{
			var result = _accountService.Login(userLoginModel);

			if (result.Status == RequestResultStatus.Success)
			{
				_accessController.SetCookie(result.Object);
				return Request.CreateResponse(HttpStatusCode.OK, result);
			}
			return Request.CreateResponse(HttpStatusCode.BadRequest, result);
		}

		[Route("Logout"), HttpPost]
		public HttpResponseMessage Logout()
		{
			_accessController.ExpireCookie();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Route("Register"), HttpPost]
		public HttpResponseMessage Register(UserRegistrationModel userRegistrationModel)
		{
			var result = _accountService.Register(userRegistrationModel);
			var statusCode = (result.Status == RequestResultStatus.Error) ? HttpStatusCode.BadRequest : HttpStatusCode.OK;
			return Request.CreateResponse(statusCode, result);
		}
		/*
		[Route("Check"), HttpPost, Authorize]
		public HttpResponseMessage Check()
		{
			return Request.CreateResponse(HttpStatusCode.OK);
		}*/
	}
}