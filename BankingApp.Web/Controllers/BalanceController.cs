﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Access;
using BankingApp.Common.Models;
using BankingApp.Common.ViewModels;
using BankingApp.Services.Interfaces;

namespace BankingApp.Web.Controllers
{
	[Authorize]
	[RoutePrefix("balance")]
	public class BalanceController : ApiController
    {
		private readonly IBalanceService _balanceService;
		private readonly IAccessController _accessController;

		public BalanceController(IBalanceService balanceService, IAccessController accessController)
		{
			_balanceService = balanceService;
			_accessController = accessController;
		}

		[Route("Get")]
		public HttpResponseMessage GetBalance()
		{
			var result = _balanceService.GetBalance(_accessController.GetUserId());
			if (result.Status == RequestResultStatus.Error)
			{
				return Request.CreateResponse(HttpStatusCode.BadRequest, result);
			}
			return Request.CreateResponse(HttpStatusCode.OK, result);
		}

		[Route("Deposit"), HttpPost]
		public HttpResponseMessage Deposit(DepositModel depositModel)
		{
			var result = _balanceService.Deposit(_accessController.GetUserId(), depositModel);
			if (result.Status == RequestResultStatus.Error)
			{
				return Request.CreateResponse(HttpStatusCode.BadRequest, result);
			}
			return Request.CreateResponse(HttpStatusCode.OK, result);
		}

		[Route("Withdraw"), HttpPost]
		public HttpResponseMessage Withdraw(WithdrawModel withdrawModel)
		{
			var result = _balanceService.Withdraw(_accessController.GetUserId(), withdrawModel);
			if (result.Status == RequestResultStatus.Error)
			{
				return Request.CreateResponse(HttpStatusCode.BadRequest, result);
			}
			return Request.CreateResponse(HttpStatusCode.OK, result);
		}

		[Route("Transfer"), HttpPost]
		public HttpResponseMessage Transfer(TransferModel transferModel)
		{
			var result = _balanceService.Transfer(_accessController.GetUserId(), transferModel);
			if (result.Status == RequestResultStatus.Error)
			{
				return Request.CreateResponse(HttpStatusCode.BadRequest, result);
			}
			return Request.CreateResponse(HttpStatusCode.OK, result);
		}
	}
}
