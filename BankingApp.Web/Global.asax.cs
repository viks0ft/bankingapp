﻿using System.Web;
using System.Web.Http;
using System.Web.Optimization;

namespace BankingApp.Web
{
	public class WebApiApplication : HttpApplication
	{
		protected void Application_Start()
		{
			GlobalConfiguration.Configure(WebApiConfig.Register);
		}
	}
}