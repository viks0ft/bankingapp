﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using BankingApp.Entities;

namespace BankingApp.DataAccess.DataContext
{
	public class DatabaseContext : DbContext
	{
		public DatabaseContext() : base("name=BankingApp")
		{
			
		}

		public virtual DbSet<Transaction> Transactions { get; set; }
		public virtual DbSet<User> Users { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
		}
	}
}