﻿using System.Collections.Generic;
using System.Linq;
using BankingApp.DataAccess.DataContext;
using BankingApp.DataAccess.Interfaces;
using BankingApp.Entities;

namespace BankingApp.DataAccess.Repositories
{
	internal class UserRepository : GenericRepository<User>, IUserRepository
	{
		public UserRepository(DatabaseContext context) : base(context)
		{
		}

		public User GetUser(int userId)
		{
			return _context.Users.FirstOrDefault(x => x.Id == userId);
		}

		public User GetUser(string userLogin, string userPassword)
		{
			return _context.Users.FirstOrDefault(x => x.Login == userLogin && x.Password == userPassword);
		}

		public bool UserExists(string userLogin)
		{
			return _context.Users.Any(x => x.Login == userLogin);
		}

		public string GetUserSalt(string userLogin)
		{
			return _context.Users.FirstOrDefault(x => x.Login == userLogin)?.Salt ?? "";
		}

		public List<User> GetUsers(int exceptId)
		{
			return _context.Users.Where(x => x.Id != exceptId).OrderBy(x => x.Login).Select(x => x).ToList();
		}
	}
}