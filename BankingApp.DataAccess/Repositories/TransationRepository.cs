﻿using System.Collections.Generic;
using System.Linq;
using BankingApp.DataAccess.DataContext;
using BankingApp.DataAccess.Interfaces;
using BankingApp.Entities;

namespace BankingApp.DataAccess.Repositories
{
	internal class TransationRepository : GenericRepository<Transaction>, ITransactionRepository
	{
		public TransationRepository(DatabaseContext context) : base(context)
		{
		}

		public List<Transaction> GetUserOutgoingTransactions(int userId)
		{
			return _context.Transactions.Where(x => x.SenderId == userId).ToList();
		}

		public List<Transaction> GetUserIncomingTransactions(int userId)
		{
			return _context.Transactions.Where(x => x.RecipientId == userId).ToList();
		}
	}
}