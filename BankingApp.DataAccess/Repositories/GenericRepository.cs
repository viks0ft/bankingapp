﻿using System.Linq;
using BankingApp.DataAccess.DataContext;
using BankingApp.DataAccess.Interfaces;

namespace BankingApp.DataAccess.Repositories
{
	public abstract class GenericRepository<T> : IGenericRepository<T> where T : class
	{
		protected DatabaseContext _context;

		protected GenericRepository(DatabaseContext context)
		{
			_context = context;
		}

		public virtual void Add(T entity)
		{
			_context.Set<T>().Add(entity);
		}

		public virtual void Delete(T entity)
		{
			_context.Set<T>().Remove(entity);
		}

		public virtual IQueryable<T> GetQuery()
		{
			return _context.Set<T>();
		}
	}
}