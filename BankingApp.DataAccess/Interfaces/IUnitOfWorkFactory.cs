﻿namespace BankingApp.DataAccess.Interfaces
{
	public interface IUnitOfWorkFactory
	{
		IUnitOfWork CreateUnitOfWork();
	}
}