﻿using System;

namespace BankingApp.DataAccess.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{
		IUserRepository UserRepository { get; }
		ITransactionRepository TransitionRepository { get; }
		void RollBack();
		void Save();
	}
}