﻿using System.Collections.Generic;
using BankingApp.Entities;

namespace BankingApp.DataAccess.Interfaces
{
	public interface IUserRepository : IGenericRepository<User>
	{
		User GetUser(int userId);
		User GetUser(string userLogin, string userPassword);
		bool UserExists(string userLogin);
		string GetUserSalt(string userLogin);
		List<User> GetUsers(int exceptId);
	}
}