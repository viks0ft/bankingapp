﻿using System.Collections.Generic;
using BankingApp.Entities;

namespace BankingApp.DataAccess.Interfaces
{
	public interface ITransactionRepository : IGenericRepository<Transaction>
	{
		List<Transaction> GetUserOutgoingTransactions(int userId);
		List<Transaction> GetUserIncomingTransactions(int userId);
	}
}