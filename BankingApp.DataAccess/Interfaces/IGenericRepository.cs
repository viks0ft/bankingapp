﻿using System.Linq;

namespace BankingApp.DataAccess.Interfaces
{
	public interface IGenericRepository<T> where T : class
	{
		void Add(T entity);
		void Delete(T entity);
		IQueryable<T> GetQuery();
	}
}