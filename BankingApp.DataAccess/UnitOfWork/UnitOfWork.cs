﻿using System.Data.Entity;
using System.Runtime.InteropServices;
using BankingApp.DataAccess.DataContext;
using BankingApp.DataAccess.Interfaces;
using BankingApp.DataAccess.Repositories;

namespace BankingApp.DataAccess.UnitOfWork
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly DatabaseContext _context;
		private readonly DbContextTransaction _transaction;
		private ITransactionRepository _transactionRepository;
		private IUserRepository _userRepository;

		public UnitOfWork()
		{
			_context = new DatabaseContext();
			_transaction = _context.Database.BeginTransaction();
		}

		public IUserRepository UserRepository => _userRepository ?? (_userRepository = new UserRepository(_context));

		public ITransactionRepository TransitionRepository
			=> _transactionRepository ?? (_transactionRepository = new TransationRepository(_context));

		public void RollBack()
		{
			_transaction.Rollback();
		}

		public void Save()
		{
			_context.SaveChanges();
			_transaction.Commit();
		}

		public void Dispose()
		{
			if (Marshal.GetExceptionCode() == 0)
				Save();
			else
				RollBack();
			_transaction.Dispose();
			_context.Dispose();
		}
	}
}