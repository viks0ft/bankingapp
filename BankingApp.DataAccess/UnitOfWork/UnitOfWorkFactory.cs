﻿using BankingApp.DataAccess.Interfaces;

namespace BankingApp.DataAccess.UnitOfWork
{
	public class UnitOfWorkFactory : IUnitOfWorkFactory
	{
		public IUnitOfWork CreateUnitOfWork()
		{
			return new UnitOfWork();
		}
	}
}