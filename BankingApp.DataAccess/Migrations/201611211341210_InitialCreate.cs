namespace BankingApp.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        SenderId = c.Int(),
                        RecipientId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.RecipientId)
                .ForeignKey("dbo.Users", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.RecipientId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 16),
                        Password = c.String(nullable: false, maxLength: 64),
                        Salt = c.String(nullable: false, maxLength: 8),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "SenderId", "dbo.Users");
            DropForeignKey("dbo.Transactions", "RecipientId", "dbo.Users");
            DropIndex("dbo.Transactions", new[] { "RecipientId" });
            DropIndex("dbo.Transactions", new[] { "SenderId" });
            DropTable("dbo.Users");
            DropTable("dbo.Transactions");
        }
    }
}
