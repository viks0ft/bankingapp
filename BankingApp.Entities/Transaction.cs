﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankingApp.Entities
{
    public class Transaction
    {
        public Transaction()
        {
            Date = DateTime.Now;
        }

		[Key]
        public int Id { get; set; }
		[Required]
        public decimal Amount { get; set; }
		[Required]
        public DateTime Date { get; set; }

		[ForeignKey("Sender")]
		public int? SenderId { get; set; }
		[ForeignKey("Recipient")]
		public int? RecipientId { get; set; }

		public virtual User Sender { get; set; }
		public virtual User Recipient { get; set; }
	}
}
