﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankingApp.Entities
{
    public class User
    {
		[Key]
        public int Id { get; set; }
		[Required, MaxLength(16), MinLength(3)]
        public string Login { get; set; }
		[Required, MaxLength(64)]
        public string Password { get; set; }
		[Required, MaxLength(8)]
        public string Salt { get; set; }
        public decimal Balance { get; set; }

		[InverseProperty("Sender")]
        public virtual ICollection<Transaction> OutgoingTransactions { get; set; }
		[InverseProperty("Recipient")]
		public virtual ICollection<Transaction> IncomingTransactions { get; set; }
    }
}
