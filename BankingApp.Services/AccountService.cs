﻿using BankingApp.Common.Models;
using BankingApp.Common.SecurityProviders;
using BankingApp.Common.ViewModels;
using BankingApp.Entities;
using BankingApp.Services.Interfaces;

namespace BankingApp.Services
{
	public class AccountService : IAccountService
	{
		private readonly IUserService _userService;
		private readonly ISecurityProvider _securityProvider;

		public AccountService(IUserService userService, ISecurityProvider securityProvider)
		{
			_userService = userService;
			_securityProvider = securityProvider;
		}

		public RequestResult<UserModel> Register(UserRegistrationModel registrationModel)
		{
			var result = new RequestResult<UserModel>();
			if(registrationModel == null)
			{
				result.Set(RequestResultStatus.Error, null, "Invalid registration model");
				return result;
			}
			if (registrationModel.Password != registrationModel.PasswordConfirm)
			{
				result.Set(RequestResultStatus.Error, null,
					"Your password and confirmation password don't match!");
				return result;
			}
			if (_userService.UserExists(registrationModel.Login))
			{
				result.Set(RequestResultStatus.Error, null,
					"User " + registrationModel.Login + " already exists. Try to pick another login.");
				return result;
			}

			var salt = _securityProvider.CreateSalt(5);
			var user = new User() {Login = registrationModel.Login, Password = _securityProvider.CreateHash(registrationModel.Password, salt), Salt = salt};

			if (_userService.CreateUser(user))
			{
				var userModel = new UserModel() {Id = user.Id, Login = user.Login};
				result.Set(RequestResultStatus.Success, userModel);
			}
			else
			{
				result.Set(RequestResultStatus.Error, null, "Database error. User can't be created at the moment.");
			}
			return result;
		}

		public RequestResult<UserModel> Login(UserLoginModel loginModel)
		{
			var result = new RequestResult<UserModel>();
			if (loginModel == null)
			{
				result.Set(RequestResultStatus.Error, null, "Invalid login model");
				return result;
			}
			var user = _userService.GetUser(loginModel.Login, _securityProvider.CreateHash(loginModel.Password, _userService.GetUserSalt(loginModel.Login)));
			if (user != null)
			{
				var userModel = new UserModel() {Id = user.Id, Login = user.Login};
				result.Set(RequestResultStatus.Success, userModel);
				return result;
			}
			result.Set(RequestResultStatus.Error, null, "Login failed");
			return result;
		}
	}
}
