﻿using System;
using System.Collections.Generic;
using System.Linq;
using BankingApp.Common.Models;
using BankingApp.Common.ViewModels;
using BankingApp.DataAccess.Interfaces;
using BankingApp.Entities;
using BankingApp.Services.Interfaces;

namespace BankingApp.Services
{
	public class BalanceService : IBalanceService
	{
		private readonly IUnitOfWorkFactory _uowFactory;
		public BalanceService(IUnitOfWorkFactory uowFactory)
		{
			_uowFactory = uowFactory;
		}
		public RequestResult<decimal> GetBalance(int userId)
		{
			var result = new RequestResult<decimal>();
			try
			{
				using (var uow = _uowFactory.CreateUnitOfWork())
				{
					decimal balance = uow.UserRepository.GetUser(userId).Balance;
					result.Set(RequestResultStatus.Success, balance);
					return result;
				}
			}
			catch (Exception)
			{
				result.Set(RequestResultStatus.Error, 0, "Database error. Please, try again later.");
				return result;
			}
		}

		public RequestResult<decimal> Deposit(int userId, DepositModel depositModel)
		{
			var result = new RequestResult<decimal>();
			if (depositModel == null || depositModel.Amount <= 0)
			{
				result.Set(RequestResultStatus.Error, 0, "Invalid deposit model");
				return result;
			}
			try
			{
				using (var uow = _uowFactory.CreateUnitOfWork())
				{
					var user = uow.UserRepository.GetUser(userId);
					user.Balance += depositModel.Amount;
					CreateTransaction(null, user, depositModel.Amount);
					result.Set(RequestResultStatus.Success, user.Balance);
					return result;
				}
			}
			catch (Exception)
			{
				result.Set(RequestResultStatus.Error, 0, "Deposit error. Please, try again later.");
				return result;
			}
		}

		public RequestResult<decimal> Withdraw(int userId, WithdrawModel withdrawModel)
		{
			var result = new RequestResult<decimal>();
			if (withdrawModel == null || withdrawModel.Amount <= 0)
			{
				result.Set(RequestResultStatus.Error, 0, "Invalid withdraw model");
				return result;
			}
			try
			{
				using (var uow = _uowFactory.CreateUnitOfWork())
				{
					var user = uow.UserRepository.GetUser(userId);
					if (user.Balance < withdrawModel.Amount)
					{
						result.Set(RequestResultStatus.Error, 0, "Not enough money");
						return result;
					}
					user.Balance -= withdrawModel.Amount;
					CreateTransaction(user, null, withdrawModel.Amount);
					result.Set(RequestResultStatus.Success, user.Balance);
					return result;
				}
			}
			catch (Exception)
			{
				result.Set(RequestResultStatus.Error, 0, "Withdraw error. Please, try again later.");
				return result;
			}
		}

		public RequestResult<decimal> Transfer(int senderId, TransferModel transferModel)
		{
			var result = new RequestResult<decimal>();
			if (transferModel == null || transferModel.Amount <= 0)
			{
				result.Set(RequestResultStatus.Error, 0, "Invalid transfer model");
				return result;
			}
			try
			{
				using (var uow = _uowFactory.CreateUnitOfWork())
				{
					var sender = uow.UserRepository.GetUser(senderId);
					var recipient = uow.UserRepository.GetUser(transferModel.RecipientId);
					if (sender.Balance < transferModel.Amount)
					{
						result.Set(RequestResultStatus.Error, 0, "Not enough money");
						return result;
					}
					sender.Balance -= transferModel.Amount;
					recipient.Balance += transferModel.Amount;
					CreateTransaction(sender, recipient, transferModel.Amount);
					result.Set(RequestResultStatus.Success, sender.Balance);
					return result;
				}
			}
			catch (Exception)
			{
				result.Set(RequestResultStatus.Error, 0, "Transfer error. Please, try again later.");
				return result;
			}
		}

		public RequestResult<List<TransactionModel>> GetTransfers(int userId)
		{
			var result = new RequestResult<List<TransactionModel>>();

			try
			{
				using (var uow = _uowFactory.CreateUnitOfWork())
				{
					var outgoingTransactions = uow.TransitionRepository.GetUserOutgoingTransactions(userId);
					var incomingTransactions = uow.TransitionRepository.GetUserIncomingTransactions(userId);
					List<TransactionModel> transactions = new List<TransactionModel>();
					foreach (var ta in outgoingTransactions)
					{
						transactions.Add(new TransactionModel() {Amount = -ta.Amount, Date = ta.Date, UserId = ta.RecipientId});
					}
					foreach (var ta in incomingTransactions)
					{
						transactions.Add(new TransactionModel() {Amount = ta.Amount, Date = ta.Date, UserId = ta.SenderId});
					}
					result.Set(RequestResultStatus.Success, transactions.OrderByDescending(x => x.Date).ToList());
				}
			}
			catch (Exception)
			{
				result.Set(RequestResultStatus.Error, null, "Can't get user transactions");
			}
			return null;
		}

		private void CreateTransaction(User sender, User recipient, decimal amount)
		{
			if (sender != null)
			{
				if (sender.OutgoingTransactions == null)
					sender.OutgoingTransactions = new List<Transaction>();
				sender.OutgoingTransactions.Add(new Transaction() {Recipient = recipient, Amount = amount});
			}
			else if (recipient != null)
			{
				if (recipient.IncomingTransactions == null)
					recipient.IncomingTransactions = new List<Transaction>();
				recipient.OutgoingTransactions.Add(new Transaction() { Amount = amount });
			}
		}
	}
}
