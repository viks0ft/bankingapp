﻿using BankingApp.Common.Models;
using BankingApp.Common.ViewModels;

namespace BankingApp.Services.Interfaces
{
	public interface IAccountService
	{
		RequestResult<UserModel> Register(UserRegistrationModel registrationModel);
		RequestResult<UserModel> Login(UserLoginModel loginModel);
	}
}
