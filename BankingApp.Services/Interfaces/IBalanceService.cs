﻿using System.Collections.Generic;
using BankingApp.Common.Models;
using BankingApp.Common.ViewModels;

namespace BankingApp.Services.Interfaces
{
	public interface IBalanceService
	{
		RequestResult<decimal> GetBalance(int userId);
		RequestResult<decimal> Deposit(int userId, DepositModel depositModel);
		RequestResult<decimal> Withdraw(int userId, WithdrawModel withdrawModel);
		RequestResult<decimal> Transfer(int senderId, TransferModel transferModel);

		RequestResult<List<TransactionModel>> GetTransfers(int userId);
	}
}
