﻿using BankingApp.Entities;

namespace BankingApp.Services.Interfaces
{
	public interface IUserService
	{
		User GetUser(int userId);
		User GetUser(string userLogin, string userPassword);
		string GetUserSalt(string userLogin);
		bool UserExists(string userLogin);
		bool CreateUser(User user);
	}
}