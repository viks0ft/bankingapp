﻿using System;
using BankingApp.DataAccess.Interfaces;
using BankingApp.Entities;
using BankingApp.Services.Interfaces;

namespace BankingApp.Services
{
	public class UserService : IUserService
	{
		private readonly IUnitOfWorkFactory _uowManager;

		public UserService(IUnitOfWorkFactory ouwManager)
		{
			_uowManager = ouwManager;
		}

		public User GetUser(int userId)
		{
			try
			{
				using (var uow = _uowManager.CreateUnitOfWork())
				{
					var user = uow.UserRepository.GetUser(userId);
					return user;
				}
			}
			catch (Exception)
			{
				return null;
			}
		}

		public User GetUser(string userLogin, string userPassword)
		{
			try
			{
				using (var uow = _uowManager.CreateUnitOfWork())
				{
					var user = uow.UserRepository.GetUser(userLogin, userPassword);
					return user;
				}
			}
			catch (Exception)
			{
				return null;
			}
		}

		public string GetUserSalt(string userLogin)
		{
			try
			{
				using (var uow = _uowManager.CreateUnitOfWork())
				{
					var salt = uow.UserRepository.GetUserSalt(userLogin);
					return salt;
				}
			}
			catch (Exception)
			{
				return null;
			}
		}

		public bool UserExists(string userLogin)
		{
			try
			{
				using (var uow = _uowManager.CreateUnitOfWork())
				{
					var exists = uow.UserRepository.UserExists(userLogin);
					return exists;
				}
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool CreateUser(User user)
		{
			try
			{
				using (var uow = _uowManager.CreateUnitOfWork())
				{
					uow.UserRepository.Add(user);
					return true;
				}
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}