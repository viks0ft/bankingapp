﻿using Microsoft.Practices.Unity;

namespace BankingApp.Common.IoC
{
	public static class IoC
	{
		private static IUnityContainer _container;

		public static IUnityContainer Instance => _container ?? (_container = new UnityContainer());

		public static T Resolve<T>()
		{
			return Instance.Resolve<T>();
		}
	}
}