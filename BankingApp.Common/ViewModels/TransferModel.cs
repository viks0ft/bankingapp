﻿namespace BankingApp.Common.ViewModels
{
	public class TransferModel
	{
		public int RecipientId { get; set; }
		public decimal Amount { get; set; }
	}
}