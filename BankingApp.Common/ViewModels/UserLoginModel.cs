﻿namespace BankingApp.Common.ViewModels
{
	public class UserLoginModel
	{
		public string Login { get; set; }
		public string Password { get; set; }
	}
}
