﻿namespace BankingApp.Common.ViewModels
{
	public class UserModel
	{
		public int Id { get; set; }
		public string Login { get; set; }
	}
}
