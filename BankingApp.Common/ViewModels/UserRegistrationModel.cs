﻿namespace BankingApp.Common.ViewModels
{
	public class UserRegistrationModel
	{
		public string Login { get; set; }
		public string Password { get; set; }
		public string PasswordConfirm { get; set; }
	}
}
