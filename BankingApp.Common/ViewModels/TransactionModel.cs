﻿using System;

namespace BankingApp.Common.ViewModels
{
	public class TransactionModel
	{
		public DateTime Date { get; set; }
		public decimal Amount { get; set; }
		public int? UserId { get; set; }
	}
}