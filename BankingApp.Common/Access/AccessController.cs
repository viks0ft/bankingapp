﻿using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using BankingApp.Common.Models;
using BankingApp.Common.ViewModels;

namespace BankingApp.Common.Access
{
	public class AccessController : IAccessController
	{
		public int GetUserId()
		{
			if (HttpContext.Current.User.Identity.IsAuthenticated)
			{
				UserPrincipal user = (UserPrincipal)HttpContext.Current.User;
				return user.Id;
			}
			return -1;
		} 

		public string GetAuthTicket(UserModel user)
		{
			var serializeModel =
				new UserPrincipalSerializeModel()
				{
					Id = user.Id,
					Login = user.Login
				};
			var serializer = new JavaScriptSerializer();
			var userData = serializer.Serialize(serializeModel);
			var authTicket = new FormsAuthenticationTicket(
				1,
				user.Login,
				DateTime.Now,
				DateTime.Now.AddMinutes(10),
				false,
				userData);
			return FormsAuthentication.Encrypt(authTicket);
		}

		public void SetCookie(UserModel user)
		{
			var authTicket = GetAuthTicket(user);
			var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, authTicket);
			HttpContext.Current.Response.SetCookie(cookie);
		}

		public void ExpireCookie()
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated) return;
			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (authCookie == null) return;
			authCookie.Expires = DateTime.MinValue;
			HttpContext.Current.Request.Cookies.Add(authCookie);
		}

		public void PostAuthenticateRequest()
		{
			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

			if (authCookie == null) return;

			var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
			var serializer = new JavaScriptSerializer();
			var serializeModel = serializer.Deserialize<UserPrincipalSerializeModel>(authTicket.UserData);
			UserPrincipal user = new UserPrincipal(serializeModel.Id, serializeModel.Login);

			HttpContext.Current.User = user;
		}
	}
}
