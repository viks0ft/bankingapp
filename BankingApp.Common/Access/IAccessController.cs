﻿using BankingApp.Common.ViewModels;

namespace BankingApp.Common.Access
{
	public interface IAccessController
	{
		int GetUserId();
		string GetAuthTicket(UserModel user);
		void SetCookie(UserModel user);
		void ExpireCookie();
		void PostAuthenticateRequest();
	}
}
