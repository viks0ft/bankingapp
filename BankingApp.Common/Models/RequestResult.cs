﻿namespace BankingApp.Common.Models
{
	public class RequestResult<T>
	{
		public RequestResultStatus Status { get; set; }
		public string Message { get; set; }
		public T Object { get; set; }
		public RequestResult()
		{
			Set(RequestResultStatus.Error, default(T), null);
		}

		public RequestResult(string message)
		{
			Set(RequestResultStatus.Error, default(T), message);
		} 

		public RequestResult(RequestResultStatus status, T obj, string message)
		{
			Set(status, obj, message);
		}

		public void Set(RequestResultStatus status, T obj)
		{
			Set(status, obj, null);
		}

		public void Set(RequestResultStatus status, T obj, string message)
		{
			Status = status;
			Object = obj;
			Message = message;
		}
	}
}
