﻿using System;
using System.Security.Principal;

namespace BankingApp.Common.Models
{
	public class UserPrincipal : IPrincipal
	{
		public int Id { get; set; }
		public string Login { get; set; }
		public IIdentity Identity { get; private set; }
		public UserPrincipal(int userId, string userLogin)
		{
			Id = userId;
			Login = userLogin;
			Identity = new GenericIdentity(Login);
		}

		public bool IsInRole(string role)
		{
			throw new NotImplementedException();
		}
	}
}
