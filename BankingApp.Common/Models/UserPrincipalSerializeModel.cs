﻿namespace BankingApp.Common.Models
{
	public class UserPrincipalSerializeModel
	{
		public int Id { get; set; }
		public string Login { get; set; }
	}
}
