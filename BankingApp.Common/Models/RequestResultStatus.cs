﻿namespace BankingApp.Common.Models
{
	public enum RequestResultStatus
	{
		Success = 0,
		Error = 1
	}
}
