﻿namespace BankingApp.Common.SecurityProviders
{
	public interface ISecurityProvider
	{
		string CreateSalt(int length);
		string CreateHash(string password, string salt);

	}
}
