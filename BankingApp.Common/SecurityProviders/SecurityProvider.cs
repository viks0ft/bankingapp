﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BankingApp.Common.SecurityProviders
{
	public class SecurityProvider : ISecurityProvider
	{
		const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		readonly Random _random = new Random();

		public string CreateSalt(int length)
		{
			var salt = "";
			for (var i = 0; i < length; i++)
			{
				salt += Chars[_random.Next(Chars.Length)];
			}
			return salt;
		}

		public string CreateHash(string password, string salt)
		{
			var sha256 = SHA256.Create();
			var inputBytes = Encoding.UTF8.GetBytes(password + salt);
			var hash = sha256.ComputeHash(inputBytes);

			var hashString = new StringBuilder();

			foreach (var b in hash)
				hashString.AppendFormat("{0:x2}", b);
			return hashString.ToString();
		}
	}
}